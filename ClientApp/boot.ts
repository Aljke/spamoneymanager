import './css/site.css';
import 'bootstrap';
import Vue from 'vue';
import VueRouter from 'vue-router';

import Datepicker from 'vuejs-datepicker';
Vue.component('datepicker', Datepicker);

Vue.use(VueRouter);

const routes = [
    { path: '/', component: require('./components/home/home.vue.html') },
    { path: '/counter', component: require('./components/counter/counter.vue.html') },
	{ path: '/fetchdata', component: require('./components/fetchdata/fetchdata.vue.html') },
	{ path: '/bills', component: require('./components/bills/bills.vue.html') }
];

new Vue({
    el: '#app-root',
    router: new VueRouter({ mode: 'history', routes: routes }),
    render: h => h(require('./components/app/app.vue.html'))
});
