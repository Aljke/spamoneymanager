﻿import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import { Datetime } from 'vue-datetime';

Vue.component('datetime', Datetime);


export class ComboboxItem {
	private value?: any;
	private text: string;

	constructor(text: string, value?: any) {
		this.Text = text;

		if (value)
			this.value = value;
	}

	public get Value(): any {
		return this.value;
	}

	public set Value(value: any) {
		this.value = value;
	}

	public get Text(): string {
		return this.text;
	}

	public set Text(value: string) {
		this.text = value;
	}
}


export class ComboboxData
{
	private comboboxSource: ComboboxItem[];
	private selectedValue: any;

	public get ComboboxSource(): ComboboxItem[] {
		return this.comboboxSource;
	}

	public set ComboboxSource(value: ComboboxItem[]) {
		this.comboboxSource = value;
		if (value.length > 0)
			this.selectedValue = this.comboboxSource[0].Value;
	}

	public get SelectedValue(): any {
		return this.selectedValue;
	}

	public set SelectedValue(value: any) {
		this.selectedValue = value;
	}
}

@Component
export default class BillsComponent extends Vue {
	private amount: number;
	private currenciesSelect: ComboboxData;
	private billDate: Date;
	private categorySelect: ComboboxData;
	private description: string;
	
	constructor() {
		super();
		this.currenciesSelect = new ComboboxData();
		this.categorySelect = new ComboboxData();

		let currencies = [
			new ComboboxItem("UAH", 1),
			new ComboboxItem("USD", 2),
			new ComboboxItem("EUR", 3)
		];

		let descriptions = [
			new ComboboxItem("Food", 1),
			new ComboboxItem("Transport", 2),
			new ComboboxItem("Wear", 3),
			new ComboboxItem("Studying", 4),
			new ComboboxItem("Holidays", 5),
			new ComboboxItem("Other", 6)
		];

		this.currenciesSelect.ComboboxSource = currencies;
		this.categorySelect.ComboboxSource = descriptions;

		this.billDate = new Date();
	}

	public get Amount(): number {
		return this.amount;
	}

	public set Amount(value:number) {
		this.amount = value;
	}

	public get CurrenciesSelect(): ComboboxData {
		return this.currenciesSelect;
	}

	public set CurrenciesSelect(value: ComboboxData) {
		this.currenciesSelect = value;
	}

	public get BillDate(): Date {
		return this.billDate;
	}

	public set BillDate(value: Date) {
		this.billDate = value;
	}

	public get CategoriesSelect(): ComboboxData {
		return this.categorySelect;
	}

	public set CategoriesSelect(value: ComboboxData) {
		this.categorySelect = value;
	}

	public get Description(): string {
		return this.description;
	}

	public set Description(value: string) {
		this.description = value;
	}
}